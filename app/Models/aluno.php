<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class aluno extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function getFaltas(){
        $vetFaltas = array();
        if ($this->exists) {
            $faltas = \App\Models\falta::where('id_aluno',$this->attributes['id'])->orderBy('mes')->get();  
            if($faltas) {
                foreach($faltas as $falta){
                    $vetFaltas[$falta->mes] = $falta->faltas;
                }
                return $vetFaltas;
            } else return null;
        } else return null;
    }

}
